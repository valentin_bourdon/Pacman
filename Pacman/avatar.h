/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET PACMAN - MTI 3D - BOURDON Valentin*/      //
//														   //
//				----- Class Avatar -----				   //
//														   //
/////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <windows.h> 
#include "SOIL/SOIL.h"
#include "GL\glut.h"


class avatar
{

private:
	int x; //Position x de l'avatar
	int y; //Position y de l'avatar
	int nbVies; //Nombre de vies du joueur
	int img; //Variable pour la texture � afficher pour l'avatar

public:
	
	int score; //Score du joueur
	avatar();

	//Fonctions qui retourne les positions x et y de l'avatar//
	int PositionX();
	int PositionY();

	//Foncction qui retourne le score du joueur et une fonction qui remet � zero le score//
	int Score();
	void ResetScore();

	//Fonctions qui met � jour le nombre de vie et une fonction qui renvoie cette valeur//
	void UpdateVies();
	int Vies();
	void ResetVie();

	//Fonction qui reset les positions de l'avatar//
	void ResetPos();

	//Fonctions de d�placements de l'avatar//
	void DepGauche();
	void DepDroite();
	void DepTop();
	void DepBottom();

	//Fonction pour afficher l'avatar//
	void Dessiner();

};


