/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET PACMAN - MTI 3D - BOURDON Valentin*/      //
//														   //
//				----- Fichier principal -----			   //
//														   //
/////////////////////////////////////////////////////////////


#include <vector>
#include <fstream>
#include <iostream>
#include <windows.h> 
#include <sstream>
#include <string>
#include "avatar.h"
#include "ennemi.h"
#include <time.h>
#include "SOIL/SOIL.h"


int LARGEUR= 1920; // Largeur de la fen�tre
int HAUTEUR = 1200; // Hauteur de la fen�tre

using namespace std;
 
int NbLignes = 22;
int NbColonnes = 22;

ifstream fichier; // Objet de type ifstream, pour lire un fichier extern

int niveau; // Varibale pour stocker le num�ro du niveau courant
char** Matrice; // Matrice contenant le niveau

vector<GLuint>	texture;

// '1' pour les murs, '0' pour le chemin', '3' pour les superBalls
/*char Matrice[22][22] =
{ '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1',
'1','0','0','3','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1',
'1','0','1','1','1','0','1','0','1','1','1','1','0','1','1','1','1','0','1','1','0','1',
'1','0','1','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','4','0','0','1',
'1','0','0','0','1','0','1','1','1','1','0','1','0','0','0','0','0','0','1','1','0','1',
'1','0','1','1','1','0','1','0','0','0','0','1','0','1','1','1','1','0','1','1','0','1',
'1','0','1','1','1','0','1','0','1','1','0','1','0','1','1','1','1','0','1','1','0','1',
'1','0','1','0','0','0','0','0','1','1','0','0','0','0','0','0','0','0','0','0','0','1',
'1','0','0','0','1','1','1','0','1','1','0','1','1','0','1','1','0','1','0','1','0','1',
'1','0','1','0','0','0','1','0','0','0','0','0','0','0','1','1','0','1','0','1','0','1',
'1','0','1','1','1','0','1','0','1','1','0','1','1','0','1','1','0','0','0','1','0','1',
'1','0','1','0','0','0','0','0','0','0','0','0','0','0','1','1','0','1','1','1','0','1',
'1','0','1','0','1','0','1','0','1','1','0','1','1','0','0','0','0','0','0','0','0','1',
'1','0','0','0','1','0','1','0','0','0','0','0','0','0','0','1','1','1','0','1','0','1',
'1','1','1','1','1','0','1','1','1','1','0','1','0','1','0','1','1','1','0','1','0','1',
'1','1','1','1','1','0','0','0','0','0','0','1','0','1','0','1','1','1','0','1','0','1',
'1','0','0','0','0','0','1','1','0','1','0','0','0','0','0','0','3','0','0','0','0','1',
'1','0','1','1','1','0','0','0','0','1','0','1','1','1','1','0','1','0','1','1','0','1',
'1','3','1','1','1','0','1','1','0','1','0','0','0','1','0','0','1','0','0','0','0','1',
'1','0','1','1','1','0','1','1','0','1','1','1','0','1','0','1','1','1','1','1','0','1',
'1','0','0','0','5','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1',
'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1', }; */


avatar pacman;
ennemi ennemi1(1,1), ennemi2(1,20), ennemi3(20,1), ennemi4(20,20);

bool pause;
bool start;
bool menu;
int startGame = 0;

bool win; // Varibale de condition de victoire
bool mange = false; // Variable pour savoir si un ennemi � �tait mang�


int gostX;  // Position x des Ghost
int gostY;  // Position y des Ghost


// Variable pour les positions x et y des t�l�porteurs pr�sents dans la matrice //
int portailAx;
int portailAy;
int portailBx;
int portailBy;


int r[4];  // Nombre al�atoire pour le d�placement des ennemis
bool takeSuperBall;  // Tag pour savoir si le joueur a r�cup�rer une super ball pour manger les ennemis
int score;  // Score du joueur
int nbBall; // Variable pour compter le nombre de boulles sur la map, pour la condition de victoire
int nbVies;  // Variable pour stocker le nombre de vies du joueur
bool again;  // Tag permettant de savoir si le joueur peut r��ssayer encore ou si son nombre de vies est insufisant
bool gameOver;  // Tag permettant de savoir que le joueur a perdu
int vitesse; // Vitesse de d�placement des ennemis


void DessinerNiveau();



// Fonction permetant d'ouvrir un fichier .txt. Fonction utile pour charger diff�rents niveaux //
void OuvrirNiveau(char* nom_fichier) {
	
	ifstream fichier; // Objet de type ifstream
	fichier.open(nom_fichier); // Ouverture du fichier

	if (!fichier) { // Test de l'existence du fichier
		cout << "Erreur lors de l'ouverture du fichier !"
			<< endl;
		system("pause");
		exit(1);
	}
	else {
		cout << "Ouverture du fichier - Succes" << nom_fichier << "\n";
	}

	// Lecture de la taille du niveau
	fichier >> NbColonnes;
	fichier >> NbLignes;
	fichier >> nbBall;

	// Allocation du tableau du niveau
	Matrice = new char*[NbColonnes];
	
	for (int i = 0; i<NbColonnes; i++)
		Matrice[i] = new char[NbLignes];

	// Initialisation des valeurs du tableau
	for (int i = 0; i<NbColonnes; i++)
		for (int j = 0; j<NbLignes; j++)
			Matrice[j][i] = '0';

	// Lecture du tableau du niveau, caract�re par caract�re
	for (int j = 0; j<NbLignes; j++)
		for (int i = 0; i<NbColonnes; i++)
			fichier >> Matrice[j][i];


	fichier.close(); // Fermeture du fichier
}


// Fonction pour afficher des caract�res dans la fen�tre // 
void vBitmapOutput(int x, int y, char *string, void *font)
{
	int len, i; // len donne la longueur de la cha�ne de caract�res

	glRasterPos2f(x, y); // Positionne le premier caract�re de la cha�ne
	len = (int)strlen(string); // Calcule la longueur de la cha�ne
	for (i = 0; i < len; i++) glutBitmapCharacter(font, string[i]); // Affiche chaque caract�re de la cha�ne
}


// Fonction pour t�l�charger des bitmap et les convertir en texture
int LoadGLTextures(string name)
{
	GLuint essai = SOIL_load_OGL_texture
		(
			name.c_str(),
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_INVERT_Y
			);

	texture.push_back(essai);

	if (texture.at(texture.size()-1) == 0)
		return false;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return true;    // Return Success
}

// Fonction pour afficher les textures dans la fenetre
void AfficheTexture(int _texture) {
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[_texture]);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(22, 22);
	glTexCoord2f(0.0f, 0.0f); glVertex2d(0, 22);
	glTexCoord2f(0.0f, 1.0f); glVertex2d(0, 0);
	glTexCoord2f(1.0f, 1.0f); glVertex2d(22, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glutPostRedisplay();
}


// TIMER ENNEMI, 4 timer, 1 pour chaque ennemi //

void timerFunc0(int value) {

	if ((pause == false)){
	srand(time(NULL));
	gostX = ennemi1.PositionX();
	gostY = ennemi1.PositionY();
	ennemi1.DeplacementEnnemi(gostX, gostY, 0, vitesse);  //passage en param�tre de la position de l'�nnemi et de la variable al�atoire r pour le d�placement de l'ennemi
	}
}
void timerFunc1(int value) {
	if ((pause == false)) {
		srand(time(NULL));
		gostX = ennemi2.PositionX();
		gostY = ennemi2.PositionY();
		ennemi2.DeplacementEnnemi(gostX, gostY, 1, vitesse);
	}
}
void timerFunc2(int value) {
	if ((pause == false)) {
		srand(time(NULL));
		gostX = ennemi3.PositionX();
		gostY = ennemi3.PositionY();
		ennemi3.DeplacementEnnemi(gostX, gostY, 2, vitesse);
	}
}
void timerFunc3(int value) {
	if ((pause == false) ) {
		srand(time(NULL));
		gostX = ennemi4.PositionX();
		gostY = ennemi4.PositionY();
		ennemi4.DeplacementEnnemi(gostX, gostY, 3, vitesse);
	}
}


/*Timer pour la gestion du pouvoir des superBall permettant a pacman de manger les ennemis, 
  une fois le timer termin�, le joueur ne peut plus manger les ennemis*/
void timerDead(int value) {
	
	takeSuperBall = false;

}

//Timer pour faire revenir les ennemis au bout de quelques secondes apr�s les avoir tu�s // 
void timerRevenirEnnemi(int value) {
	if (ennemi1.Mort() == true) {
		ennemi1.ResetPos(1, 1);
		ennemi1.UpdateVivant();
	}
	else if (ennemi2.Mort() == true) {
		ennemi2.ResetPos(1, 20);
		ennemi2.UpdateVivant();
	}
	else if (ennemi3.Mort() == true) {
		ennemi3.ResetPos(20, 1);
		ennemi3.UpdateVivant();
	}
	else if (ennemi4.Mort() == true) {
		ennemi4.ResetPos(20, 20);
		ennemi4.UpdateVivant();
	}
}

// Fonction qui g�re le d�placement des ennemis, pour lancer et stopper celui-ci si le jeu n'est pas lanc�
void DeplacementEnnemi(bool _start, bool _pause) {

	if (niveau == 1) {
		vitesse = 1000;
	}
	else if (niveau == 2) {
		vitesse = 700;
	}
	else if (niveau == 3) {
		vitesse = 400;
	}
	else if (niveau == 4) {
		vitesse = 100;
	}

	if ((_start == true) && (_pause==false)) {

		//Gestion des timer poour les d�plcaments des ennemis. Un timer pour chaque ennemis //
		glutTimerFunc(vitesse, timerFunc0, 0);
		glutTimerFunc(vitesse, timerFunc1, 0);
		glutTimerFunc(vitesse, timerFunc2, 0);
		glutTimerFunc(vitesse, timerFunc3, 0);


	}

}


void LabyAffichage()
{
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	////////////////////////	Affiche le niveau	////////////////////////
	
	DessinerNiveau();

	glFlush();

}

// Fonction pour recommencer si le joueur � encore des vies ou afficher l'�cran de game over si le nombre de vie = 0
void tryAgain() {

	pacman.UpdateVies();
	nbVies = pacman.Vies();

	// MORT //
	if (nbVies == 0) {
		start = false;
		gameOver = true;
		win = false;
		PlaySound(NULL, 0, 0); // arr�te le son pr�c�dent
		sndPlaySound(TEXT("sound/GameOver.wav"), SND_ASYNC | SND_NOSTOP); // joue un son
	}
	else {
		
		pacman.ResetPos();
		ennemi1.ResetPos(1, 1);
		ennemi1.UpdateVivant();
		ennemi2.ResetPos(1, 20);
		ennemi2.UpdateVivant();
		ennemi3.ResetPos(20, 1);
		ennemi3.UpdateVivant();
		ennemi4.ResetPos(20, 20);
		ennemi4.UpdateVivant();
	}


}

// Fonction qui remet tout � zero lorsque le joueur recommence une partie.
void reset() {
	
	takeSuperBall = false;
	pause = false;
	start = true;
	menu = false;
	win = false;
	again = false;
	gameOver = false;

	pacman.ResetVie();
	nbVies = pacman.Vies();
	pacman.ResetPos();
	pacman.ResetScore();

	ennemi1.ResetPos(1, 1);
	ennemi1.UpdateVivant();
	ennemi2.ResetPos(1, 20);
	ennemi2.UpdateVivant();
	ennemi3.ResetPos(20, 1);
	ennemi3.UpdateVivant();
	ennemi4.ResetPos(20, 20);
	ennemi4.UpdateVivant();
	
	niveau = 1;
	OuvrirNiveau("niveau1.txt"); // Ouverture de "niveau1.txt"

}

// Fonction qui affiche le background du jeu
void afficheBackground() {
	//////AFFICHAGE DU BACKGROUND ///
	glViewport(0, 100, LARGEUR, HAUTEUR-100);
	glLoadIdentity();

	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture[19]);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(22, 22);
	glTexCoord2f(0.0f, 0.0f); glVertex2d(0, 22);
	glTexCoord2f(0.0f, 1.0f); glVertex2d(0, 0);
	glTexCoord2f(1.0f, 1.0f); glVertex2d(22, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glutPostRedisplay();
}


// Fonctions pour la gestion des touches du clavier et des diff�rents �v�nements
void traitementClavier2(unsigned char key, int x, int y)
{
	
	if ((key == 32) && (gameOver == false) && (again == false) && (startGame == 0) && (menu==true) && (start==false)) {  //touche espace
		start = true;
		startGame = 1;
		DeplacementEnnemi(start,pause);
	}
	if (key == 13) {  //touche entr�e
		menu = true;
	}

	if ((key == 27) && (gameOver == false) && (win==false) && (startGame ==1)) {  //touche echap
		pause = !pause;
		start = !start;
		DeplacementEnnemi(start,pause);
		PlaySound(NULL, 0, 0);
		
	}

	if ((key == 13) && (gameOver == true)) {  //touche entr�e
		reset();
	}



}
void traitementClavier(int key, int x, int y)
{
	if (pause == false) {
		if (key == GLUT_KEY_LEFT) pacman.DepGauche();
		if (key == GLUT_KEY_RIGHT) pacman.DepDroite();
		if (key == GLUT_KEY_UP) pacman.DepTop();
		if (key == GLUT_KEY_DOWN)pacman.DepBottom();
	}

}


void DessinerNiveau(){

	
	
	//AFFICHAGE DU NIVEAU DE JEU //
	if ((start == true) && (win==false) && (nbVies>0) && (again==false) && (pause==false)) {
	
		sndPlaySound(TEXT("sound/pacman_beginning.wav"), SND_ASYNC | SND_NOSTOP); // Joue un son
		
		afficheBackground();

		glViewport(LARGEUR/4, HAUTEUR/6, LARGEUR*0.5 , HAUTEUR*0.6);
		glLoadIdentity();
		
		// Stockage des coordonn�es des portails de t�l�portation //
		for (int i = 0; i < 22; i++) {
			for (int j = 0; j < 22; j++) {
				if (Matrice[i][j] == '5') {
					portailAx = i;
					portailAy = j;
				}
				if (Matrice[i][j] == '4') {
					portailBx = i;
					portailBy = j;
				}
			}
		}

		////Affichage de la map /////

		int i = 0, j = 0;
		for (i = 0; i < 22; i++)
		{
			for (j = 0; j < 22; j++)
			{
				switch (Matrice[j][i])
				{
					////////////////////////	 utiliser un vecteur qui contient les textures afin d'aleger le code	////////////////////////
				case '0': // balls
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, texture[3]);
					glBegin(GL_QUADS);
					glColor3d(1.0, 1.0, 1.0);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
					glutPostRedisplay();
					break;

				case '1': // mur
					glPushMatrix();

					
					glBegin(GL_QUADS);
					glColor4d(0.0, 0.0, 1.0,0.3);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					

					glPopMatrix();
					glutPostRedisplay();
					break;

				case '2': // sol

					glPushMatrix();

					
					glBegin(GL_QUADS);
					glColor3d(0.0, 0.0, 0.0);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					

					glPopMatrix();
					glutPostRedisplay();
					break;

				case '3': //SuperBall

					
					glPushMatrix();

					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, texture[15]);
					glBegin(GL_QUADS);
					glColor3d(1.0, 1.0, 1.0);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					glDisable(GL_TEXTURE_2D);

					glPopMatrix();
					glutPostRedisplay();
					break;

				case '4': //Portail / T�l�portation

					glPushMatrix();

					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, texture[21]);
					glBegin(GL_QUADS);
					glColor3d(1.0, 1.0, 1.0);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					glDisable(GL_TEXTURE_2D);

					glPopMatrix();
					glutPostRedisplay();
					break;

				case '5': //Portail / T�l�portation

					glPushMatrix();

					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, texture[21]);
					glBegin(GL_QUADS);
					glColor3d(1.0, 1.0, 1.0);
					glTexCoord2f(0.0f, 0.0f); glVertex2d(i, j);
					glTexCoord2f(1.0f, 0.0f); glVertex2d(i + 1, j);
					glTexCoord2f(1.0f, 1.0f); glVertex2d(i + 1, j + 1);
					glTexCoord2f(0.0f, 1.0f); glVertex2d(i, j + 1);
					glEnd();
					glDisable(GL_TEXTURE_2D);

					glPopMatrix();
					glutPostRedisplay();
					break;
				}
			}
		}
	
		//AFFICHE DE PACMAN ET DES GHOSTS//	
		
		pacman.Dessiner();

		if (ennemi1.Mort() == false) {  //Affiche l'ennemi si celui est vivant
			ennemi1.Dessiner(4);
		}
		if (ennemi2.Mort() == false) {
			ennemi2.Dessiner(5);
		}
		if (ennemi3.Mort() == false) {
			ennemi3.Dessiner(6);
		}
		if (ennemi4.Mort() == false) {
			ennemi4.Dessiner(7);
		}
		
		//GESTION DE LA MORT DES ENNEMIS //

		if ((takeSuperBall == true) && (pacman.PositionX() == ennemi1.PositionX()) && (pacman.PositionY() == ennemi1.PositionY())) {
			mange = true;
			ennemi1.UpdateMort();
			glutTimerFunc(5000, timerRevenirEnnemi, 0);
		}
		else if ((takeSuperBall == true) && (pacman.PositionX() == ennemi2.PositionX()) && (pacman.PositionY() == ennemi2.PositionY())) {
			mange = true;
			ennemi2.UpdateMort();
			glutTimerFunc(5000, timerRevenirEnnemi, 0);
		}
		else if ((takeSuperBall == true) && (pacman.PositionX() == ennemi3.PositionX()) && (pacman.PositionY() == ennemi3.PositionY())) {
			mange = true;
			ennemi3.UpdateMort();
			glutTimerFunc(5000, timerRevenirEnnemi, 0);
		}
		else if ((takeSuperBall == true) && (pacman.PositionX() == ennemi4.PositionX()) && (pacman.PositionY() == ennemi4.PositionY())) {
			mange = true;
			ennemi4.UpdateMort();
			glutTimerFunc(5000, timerRevenirEnnemi, 0);
		}
		else if ((takeSuperBall == false) && (((pacman.PositionX() == ennemi1.PositionX()) && (pacman.PositionY() == ennemi1.PositionY()) && (ennemi1.Mort() == false)) ||
			((pacman.PositionX() == ennemi2.PositionX()) && (pacman.PositionY() == ennemi2.PositionY()) && (ennemi2.Mort()==false)) ||
			((pacman.PositionX() == ennemi3.PositionX()) && (pacman.PositionY() == ennemi3.PositionY()) && (ennemi3.Mort() == false)) ||
			((pacman.PositionX() == ennemi4.PositionX()) && (pacman.PositionY() == ennemi4.PositionY()) && (ennemi4.Mort() == false)))) {

			PlaySound(NULL, 0, 0);
			sndPlaySound(TEXT("sound/pacman_death.wav"), SND_ASYNC | SND_NOSTOP);
			
			// Nouvel ESSAI //
			Sleep(1000); // Temps d'attente avant de relancer
			tryAgain();
		}
		

		// AFFICHAGE DU SCORE //

		glViewport(LARGEUR/3, 0, LARGEUR/3, HAUTEUR/7);
		glLoadIdentity();
		
		glColor3d(1, 1, 1);
		vBitmapOutput(0, 8, "Score : ", GLUT_BITMAP_HELVETICA_18);  //Affichage d'un texte
		
		string TotalScore = static_cast<ostringstream*>(&(ostringstream() << pacman.Score()))->str();  //Conversion de score (int) en une chaine de caract�re//
		char* s;
		s = (char*)TotalScore.c_str();	//Conversion d'une chaine de caract�re (string) en caract�re (char)
		vBitmapOutput(3, 8, s, GLUT_BITMAP_HELVETICA_18);  //Affichage d'un texte

		//	AFFICHAGE DU NUMERO DU NIVEAU //
		
		glColor3d(1, 1, 1);
		vBitmapOutput(18, 8, "NIVEAU ", GLUT_BITMAP_HELVETICA_18);  //Affichage d'un texte

		string numNiveau = static_cast<ostringstream*>(&(ostringstream() << niveau))->str();  //Conversion de score (int) en une chaine de caract�re//
		char* n;
		n = (char*)numNiveau.c_str();	//Conversion d'une chaine de caract�re (string) en caract�re (char)
		vBitmapOutput(22, 8, n, GLUT_BITMAP_HELVETICA_18);  //Affichage d'un texte



		//	AFFICHAGE DES VIES	//

		glColor3d(1, 1, 1); 
		vBitmapOutput(8, 8, "Vies = ", GLUT_BITMAP_HELVETICA_18);  //Affichage d'un texte

		for (int i = 0; i < nbVies; i++) {
			glPushMatrix();

			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, texture[2]);
			glBegin(GL_QUADS);
			glColor3d(1.0, 1.0, 1.0);
			glTexCoord2f(0.0f, 1.0f); glVertex2d(11+i, 8.75);
			glTexCoord2f(1.0f, 1.0f); glVertex2d(12+i, 8.75);
			glTexCoord2f(1.0f, 0.0f); glVertex2d(12+i, 3.75);
			glTexCoord2f(0.0f, 0.0f); glVertex2d(11+i, 3.75);
			glEnd();
			glDisable(GL_TEXTURE_2D);

			glPopMatrix();
			glutPostRedisplay();

		}
		
		//GESTION DES SUPER BALLS //

		if (takeSuperBall == true) {
			glutTimerFunc(5000, timerDead, 0);
		}

		// VICTOIRE //
		if (nbBall == 0) {
			win = true;
			start = false;
			cout << "Win : " << win;	
		}
	}

	//AFFICHAGE DU MENU DU JEU //
	else if((start==false) && (again==false) && (gameOver==false) && (menu==false) && (pause == false)) {
		
		//PlaySound(TEXT("pacman_beginning.wav"), NULL, SND_FILENAME | SND_LOOP);
		glViewport(0, 0, LARGEUR, HAUTEUR);
		glLoadIdentity();
	
		AfficheTexture(9);
	}

	//AFFICHE DU MENU DE DEPLACEMENT//
	else if ((start == false) && (again == false) && (gameOver == false) && (menu == true) && (pause == false) && (win==false)) {
		//PlaySound(TEXT("pacman_beginning.wav"), NULL, SND_FILENAME | SND_LOOP);
		glViewport(0, 0, LARGEUR, HAUTEUR);
		glLoadIdentity();

		AfficheTexture(12);
	}

	//AFFICHAGE DE L'ECRAN DE GAME OVER SI LE JOUEUR N'A PLUS DE VIE //
	else if((gameOver==true) && (start==false) && (pause == false) && (win==false)){
		
		afficheBackground();
		
		//AFFICHAGE DE L'ECRAN GAME OVER //
		
		glViewport(LARGEUR / 3, HAUTEUR / 3, LARGEUR*0.33, HAUTEUR*0.3);
		glLoadIdentity();
		AfficheTexture(10);

		//	AFFICHAGE DU SCORE	//

		glViewport(LARGEUR / 2.5, HAUTEUR/4, LARGEUR / 11, HAUTEUR / 11);
		glLoadIdentity();
		AfficheTexture(22);
		
		glViewport(LARGEUR / 2, HAUTEUR / 4, LARGEUR / 5, HAUTEUR / 5);
		glLoadIdentity();

		string TotalScore = static_cast<ostringstream*>(&(ostringstream() << pacman.Score()))->str();  //Conversion de score (int) en une chaine de caract�re//
		char* s;
		s = (char*)TotalScore.c_str();	//Conversion d'une chaine de caract�re (string) en caract�re (char)
		vBitmapOutput(3, 18, s, GLUT_BITMAP_TIMES_ROMAN_24);  //Affichage d'un texte
	}  
	else if ((win == true) && (start==false)) {
	
			afficheBackground();


			// Si le joueur est arriv� au dernier niveau du jeu est qu'il gagne, on affiche l'ecran de victoire
			if ((niveau == 2) && (win == true)) {
				glViewport(LARGEUR / 3, HAUTEUR / 3, LARGEUR*0.33, HAUTEUR*0.3);
				glLoadIdentity();
				AfficheTexture(14);

				//	AFFICHAGE DU SCORE	//

				glViewport(LARGEUR / 2.5, HAUTEUR / 4, LARGEUR / 11, HAUTEUR / 11);
				glLoadIdentity();
				AfficheTexture(22);


				glViewport(LARGEUR / 2, HAUTEUR / 4, LARGEUR / 5, HAUTEUR / 5);
				glLoadIdentity();

				string TotalScore = static_cast<ostringstream*>(&(ostringstream() << pacman.Score()))->str();  //Conversion de score (int) en une chaine de caract�re//
				char* s;
				s = (char*)TotalScore.c_str();	//Conversion d'une chaine de caract�re (string) en caract�re (char)
				vBitmapOutput(3, 18, s, GLUT_BITMAP_TIMES_ROMAN_24);  //Affichage d'un texte
			}
			// Si le joueur gagne un niveau, on t�l�charge le niveau suivant
			else {
				Sleep(3000);

				win = false;
				niveau++;

				switch (niveau) {
				case 1: OuvrirNiveau("niveaux/niveau1.txt"); // Ouverture de "niveau1.txt"
					break;
				case 2: OuvrirNiveau("niveaux/niveau2.txt"); // Ouverture de "niveau2.txt"
					break;
				}

				start = true;
				pacman.ResetPos();
				ennemi1.ResetPos(1, 1);
				ennemi1.UpdateVivant();
				ennemi2.ResetPos(1, 20);
				ennemi2.UpdateVivant();
				ennemi3.ResetPos(20, 1);
				ennemi3.UpdateVivant();
				ennemi4.ResetPos(20, 20);
				ennemi4.UpdateVivant();
			}


	}
	// SI LE JOUEUR MET LE JEU EN PAUSE // 
	else if (pause == true) {
		afficheBackground();

		//AFFICHAGE DE L'ECRAN DE PAUSE //

		glViewport(LARGEUR/3, HAUTEUR/3, LARGEUR*0.33, HAUTEUR*0.3);
		glLoadIdentity();

		AfficheTexture(20);
	}

}

void LabyRedim(int x, int y)
{
	glViewport(0, 0, x, y);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, double(NbLignes), double(NbColonnes), 0.0);
	LARGEUR = x;
	HAUTEUR = y;
}

void main()
{
	Matrice = NULL;

	PlaySound(TEXT("sound/pacman_beginning.wav"), NULL, SND_ASYNC | SND_LOOP);

	// Random d�placement ennemi //
	for (int i = 0; i < 4; i++) {
		r[i] = rand()% 4 + 1; 
	}


	takeSuperBall = false;
	pause = false;
	start = false;
	menu = false;
	win = false;
	nbVies = pacman.Vies();
	again = false;
	gameOver = false;
	
	
	niveau = 1;
	OuvrirNiveau("niveaux/niveau1.txt"); // Ouverture de "niveau1.txt"

	// Gestion de la fen�tre
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(LARGEUR, HAUTEUR);
	glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
	glutCreateWindow("Labyrinthe");
	//glutFullScreen();
	glutSpecialFunc(traitementClavier);
	glutKeyboardFunc(traitementClavier2);

	LoadGLTextures("textures/sol.png"); //0
	LoadGLTextures("textures/mur.png"); //1
	LoadGLTextures("textures/pacman_droite.png"); //2
	LoadGLTextures("textures/balls.png"); //3
	LoadGLTextures("textures/ghost.png"); //4
	LoadGLTextures("textures/ghost2.png"); //5
	LoadGLTextures("textures/ghost3.png"); //6
	LoadGLTextures("textures/ghost4.png"); //7
	LoadGLTextures("textures/ghostSeek.png"); //8
	LoadGLTextures("textures/Pac-Man_wallpaper.png"); //9
	LoadGLTextures("textures/GameOver.png"); //10
	LoadGLTextures("textures/TryAgain.png"); //11
	LoadGLTextures("textures/Pac-Man_touche.png"); //12
	LoadGLTextures("textures/map.png"); //13
	LoadGLTextures("textures/Win.png"); //14
	LoadGLTextures("textures/superBall.png"); //15
	LoadGLTextures("textures/pacman_haut.png"); //16
	LoadGLTextures("textures/pacman_gauche.png"); //17
	LoadGLTextures("textures/pacman_bas.png"); //18
	LoadGLTextures("textures/background.png"); //19
	LoadGLTextures("textures/Pause.png"); //20
	LoadGLTextures("textures/portail.png"); //21
	LoadGLTextures("textures/Score.png"); //22

	// Gestion des �v�nement

	glutDisplayFunc(LabyAffichage);
	glutReshapeFunc(LabyRedim);

	glutMainLoop();
}







