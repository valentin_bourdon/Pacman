/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET PACMAN - MTI 3D - BOURDON Valentin*/      //
//														   //
//				----- Class Ennemi -----				   //
//														   //
/////////////////////////////////////////////////////////////

#include "ennemi.h"

using namespace std;

extern char** Matrice;
//extern ennemi ennemi1, ennemi2, ennemi3, ennemi4;
extern int NbColonnes;
extern int NbLignes;

//On r�cup�re la variable takeSuperBall pour savoir si l'avatar a manger une superBall et si il peut manger un ennemi
extern bool takeSuperBall;

// On r�cup�re les fonctions timer pour g�rer le d�placements al�atoire des ennemis ind�pendemment
extern void timerFunc0(int value);
extern void timerFunc1(int value);
extern void timerFunc2(int value);
extern void timerFunc3(int value);


extern int r[4]; // On r�cup�re le tableau d'entier qui sert pour le d�placements al�atoire des ennemis

extern vector<GLuint> texture; // On r�cup�re les textures pour afficher une texture diff�rente � chaque ennemi


//Constructeur, initialise la position de d�part de l'ennemi et le met � l'�tat de 'vivant'//
ennemi::ennemi(int abs,int ord)
{
	x = abs;
	y = ord;
	dead = false;	
}

// Fonction pour passer l'ennemi � l'�tat de 'mort' ou 'vivant'//
void ennemi::UpdateMort() {
	dead = true;
}
void ennemi::UpdateVivant() {
	dead = false;
}

// Retourne un bool�en pour savoir si l'ennemi est mort ou non //
bool ennemi::Mort()
{
	return dead;
}

// Retourne la position X et Y des ennemis //
int ennemi::PositionX()
{
	return x;
}
int ennemi::PositionY()
{
	return y;
}

// D�placement de l'�nnemi //
void ennemi::DepGauche()
{	
	if ( x >1) {
		x--;
	}
}
void ennemi::DepDroite()
{
	if (x < 20) {	
		x++;
	}
}
void ennemi::DepTop()
{
	if ( y > 1 ) {	
		y--;	
	}
}
void ennemi::DepBottom()
{
	if ( y < 20) {
		y++;
	}
}

// Reset de la position de l'�nnemi //
void ennemi::ResetPos(int abs, int ord) {
	x = abs;
	y = ord;
}

//Affichage de l'ennemi //
void ennemi::Dessiner(int tex)
{

	if (takeSuperBall == false) {

		glPushMatrix();

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, texture[tex]);

		glBegin(GL_QUADS);
		glColor3d(1.0, 1.0, 1.0);
		glTexCoord2f(1.0f, 1.0f); glVertex2d(x, y);
		glTexCoord2f(0.0f, 1.0f); glVertex2d(x + 1, y);
		glTexCoord2f(0.0f, 0.0f); glVertex2d(x + 1, y+1);
		glTexCoord2f(1.0f, 0.0f); glVertex2d(x, y + 1);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glPopMatrix();
		glutPostRedisplay();
	}
	else if ((takeSuperBall == true) && (Mort() == false)) {
		glPushMatrix();

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, texture[8]);

		glBegin(GL_QUADS);
		glColor3d(1.0, 1.0, 1.0);
		glTexCoord2f(1.0f, 1.0f); glVertex2d(x, y);
		glTexCoord2f(0.0f, 1.0f); glVertex2d(x + 1, y);
		glTexCoord2f(0.0f, 0.0f); glVertex2d(x + 1, y + 1);
		glTexCoord2f(1.0f, 0.0f); glVertex2d(x, y + 1);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glPopMatrix();
		glutPostRedisplay();
	}

}

// D�placement all�atoire de l'ennemi //
void ennemi::DeplacementEnnemi(int abs, int ord, int i, int _vitesse)
{
		x = abs;
		y = ord;

	//DEPLACEMENT DROITE//
	if (r[i] == 1) {
		if ((Matrice[y - 1][x] == '1') && (Matrice[y + 1][x] == '1') && (Matrice[y][x + 1] != '1' )) { //DROITE
			DepDroite();
			r[i] = 1;
			switch (i) {
			case 0 :
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1 :
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}
			
		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x] == '1') && (Matrice[y][x + 1]  != '1' )) { //DROITE + HAUT
			r[i] = rand() % 3 + 1;
			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y - 1][x] == '1') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x + 1]  != '1' )) { //DROITE + BAS

			do {
				r[i] = rand() % 4 + 1;

			} while (r[i] == 2);

			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}

		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x + 1]  != '1' )) { //HAUT + BAS + DROITE

			r[i] = rand() % 4 + 1;
			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else if (r[i] == 2) {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}

		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x + 1] == '1')) { //HAUT + BAS
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1);

			if (r[i] == 2) {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}

		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x] == '1') && (Matrice[y][x + 1] == '1')) { //HAUT

			DepTop();
			r[i] = 2;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}

		}
		else if ((Matrice[y - 1][x] == '1') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x + 1] == '1')) { //BAS

			DepBottom();
			r[i] = 3;

			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}

		}

	}

	//DEPLACEMENT EN HAUT//

	else if (r[i] == 2) {
		if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y][x + 1] == '1') && (Matrice[y][x - 1] == '1')) { //HAUT
			DepTop();
			r[i] = 2;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}
		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y][x + 1]  != '1' ) && (Matrice[y][x - 1] == '1')) { //HAUT + DROITE
			r[i] = rand() % 3 + 1;
			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y][x + 1] == '1') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { //HAUT + GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 3);

			if (r[i] == 2) {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y - 1][x] == '1') && (Matrice[y][x + 1]  != '1' ) && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { // GAUCHE + DROITE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2 || r[i] == 3);

			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y - 1][x] == '1') && (Matrice[y][x + 1] == '1') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { //GAUCHE

			DepGauche();
			r[i] = 4;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}

		}
		else if ((Matrice[y - 1][x] == '1') && (Matrice[y][x + 1]  != '1' ) && (Matrice[y][x - 1] == '1')) { //DROITE
			DepDroite();
			r[i] = 1;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}
		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y][x + 1]  != '1' ) && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { //DROITE + GAUCHE + HAUT
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 3);

			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else if (r[i] == 2) {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}

	}


	//DEPLACEMENT GAUCHE//

	else if (r[i] == 4) {
		if ((Matrice[y - 1][x] == '1') && (Matrice[y + 1][x] == '1') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { //GAUCHE
			DepGauche();
			r[i] = 4;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}
		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x] == '1') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { //GAUCHE + HAUT
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 3);

			if (r[i] == 4) {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y - 1][x] == '1') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { //GAUCHE + BAS
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 2);

			if (r[i] == 4) {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1] == '1')) { //HAUT + BAS
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1);

			if (r[i] == 2) {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x] == '1') && (Matrice[y][x - 1] == '1')) { //HAUT

			DepTop();
			r[i] = 2;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}

		}
		else if ((Matrice[y - 1][x] == '1') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1] == '1')) { //BAS

			DepBottom();
			r[i] = 3;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}

		}
		else if ((Matrice[y - 1][x]  != '1' || Matrice[y - 1][x] == '2') && (Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2')) { //BAS+HAUT+GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1);

			if (r[i] == 2) {
				DepTop();
				r[i] = 2;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else if (r[i] == 3) {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}

		}

	}


	//DEPLACEMENT BAS //

	else if (r[i] == 3) {
		if ((Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1] == '1') && (Matrice[y][x + 1] == '1')) { //BAS
			DepBottom();
			r[i] = 3;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}
		}
		else if ((Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1] == '1') && (Matrice[y][x + 1]  != '1' )) { //BAS + DROITE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2);

			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2') && (Matrice[y][x + 1] == '1')) { //BAS + GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 1 || r[i] == 2);

			if (r[i] == 3) {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y + 1][x] == '1') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2') && (Matrice[y][x + 1]  != '1' )) { //DROITE + GAUCHE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2 || r[i] == 3);

			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}
		else if ((Matrice[y + 1][x] == '1') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2') && (Matrice[y][x + 1] == '1')) { //GAUCHE
			DepGauche();
			r[i] = 4;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}

		}
		else if ((Matrice[y + 1][x] == '1') && (Matrice[y][x - 1] == '1') && (Matrice[y][x + 1]  != '1' )) { //DROITE
			DepDroite();
			r[i] = 1;
			switch (i) {
			case 0:
				glutTimerFunc(_vitesse, timerFunc0, 0);
				break;
			case 1:
				glutTimerFunc(_vitesse, timerFunc1, 0);
				break;
			case 2:
				glutTimerFunc(_vitesse, timerFunc2, 0);
				break;
			case 3:
				glutTimerFunc(_vitesse, timerFunc3, 0);
				break;
			}

		}
		else if ((Matrice[y + 1][x]  != '1' || Matrice[y + 1][x] == '2') && (Matrice[y][x - 1]  != '1' || Matrice[y][x - 1] == '2') && (Matrice[y][x + 1]  != '1' )) { //BAS + GAUCHE + DROITE
			do {
				r[i] = rand() % 4 + 1;
			} while (r[i] == 2);

			if (r[i] == 1) {
				DepDroite();
				r[i] = 1;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else if (r[i] == 3) {
				DepBottom();
				r[i] = 3;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
			else {
				DepGauche();
				r[i] = 4;
				switch (i) {
				case 0:
					glutTimerFunc(_vitesse, timerFunc0, 0);
					break;
				case 1:
					glutTimerFunc(_vitesse, timerFunc1, 0);
					break;
				case 2:
					glutTimerFunc(_vitesse, timerFunc2, 0);
					break;
				case 3:
					glutTimerFunc(_vitesse, timerFunc3, 0);
					break;
				}
			}
		}

	}



}