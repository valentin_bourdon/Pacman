/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET PACMAN - MTI 3D - BOURDON Valentin*/      //
//														   //
//				----- Class Avatar -----				   //
//														   //
/////////////////////////////////////////////////////////////

#include "avatar.h"

using namespace std;

extern char** Matrice;
extern int NbColonnes;
extern int NbLignes;

//On r�cup�re la variable takeSuperBall pour savoir si l'avatar a manger une superBall et si il peut manger un ennemi
extern bool takeSuperBall;

//On r�cup�re les variables de position des deux t�l�porteurs pr�sent dans la matrice
extern int portailAx;
extern int portailAy;
extern int portailBx;
extern int portailBy;

extern int nbBall; // Le nombre de balls pr�sent dans la matrice
extern bool mange; // Varibale pour savoir si le joueur � mang� un ennemi

extern bool start; // On r�cup�re la variable start pour savoir si le joueur peut se d�plcaer ou non (bloque le d�placement)

extern vector<GLuint> texture; //On r�cup�re les diff�rentes textures pour modifier celle du joueur suivant son d�placement

avatar::avatar()
{
	x = 10;
	y = 10;
	score = 0;
	nbVies = 3;
	img = 2;
		
}

int avatar::PositionX() //Retourne la position x de l'avatar
{
	return x;
}

int avatar::PositionY() //Retourne la position y de l'avatar
{
	return y;
}

void avatar::ResetScore() { //On remet le score � 0 lorsque le joueur relance une partie
	score = 0;
}

int avatar::Score() //Retourne le score du joueur
{
	return score;
}

void avatar::ResetVie() { // On remet le nombre de vies initiale lorsque le joueur relance une partie
	nbVies = 3;
}

void avatar::UpdateVies() //Met � jour le nombre de vie du joueur
{
	nbVies--;
}

int avatar::Vies() //Retourne le nombre de vie du joueur
{
	return nbVies;
}

void avatar::ResetPos() //Reset les positions de l'avatar. Fonction utilis� lorsque le joueur et touch� par un ennemi ou lorsqu'il relance une partie
{
	x = 10;
	y = 10;
}


// Fonctions de d�placements de l'avatar //

void avatar::DepGauche()
{
	img = 2; //On r�cup�re la valeur de la texture de l'avatar lorsqu'il va dans la direction gauche

	if ((Matrice[y][x - 1] != '1') && (x > 0) && (start==true)) {
		x--;
		if (Matrice[y][x] == '0') {
			score++;
			nbBall--;
			Matrice[y][x] = '2';
		}
		if (Matrice[y][x] == '3') {
			takeSuperBall = true;
			Matrice[y][x] = '2';
		}
		/*Gestion de la t�l�portation, d'un point A vers un point B. On donne les nouvelles coordonn�es a notre avatar.
		On inverse les coordonn�es x et y car elle sont invers�e par rapport � celle de la matrice. 
		Si l'avatar passe sur le point B dont la matrice vaut 4, il se retrouve au point A dont la matrice vaut 5.
		Vice-versa entre les deux points*/
		if (Matrice[y][x] == '4') {
			x = portailAy + 1;  /*On d�cale d'une case pour que l'avatar ne fasse pas un aller-retour sans cesse ! Sinon, l'avatar arrive sur le second
								point de t�l�portation et repart direct au premier point.*/
			y = portailAx ;
		}
		if (Matrice[y][x] == '5') {
			x = portailBy + 1;  /*On d�cale d'une case pour que l'avatar ne fasse pas un aller-retour sans cesse ! Sinon, l'avatar arrive sur le second
								point de t�l�portation et repart direct au premier point.*/
			y = portailBx ;
		}
		// Incr�mentation du score si le joueur mange un ennemi 
		if (mange == true) {
			score += 100;
			mange = false;
		}
	}
	

	
		
}

void avatar::DepDroite()
{
	img = 17; //On r�cup�re la valeur de la texture de l'avatar lorsqu'il va dans la direction droite

	if ((Matrice[y][x + 1] != '1')  && (x < 21) && (start == true)) {
		x++;
		if (Matrice[y][x] == '0') {
			score++;
			nbBall--;
			Matrice[y][x] = '2';
		}
		if (Matrice[y][x] == '3') {
			takeSuperBall = true;
			Matrice[y][x] = '2';
		}
		//Gestion des coordonn�es de l'avatar pour la t�l�portation
		if (Matrice[y][x] == '4') {
			x = portailAy + 1;
			y = portailAx;
		}
		if (Matrice[y][x] == '5') {
			x = portailBy + 1;  //
			y = portailBx ;
		}
		// Incr�mentation du score si le joueur mange un ennemi 
		if (mange == true) {
			score += 100;
			mange = false;
		}
		
	}
	
	
	
}

void avatar::DepTop()
{
	img = 16; //On r�cup�re la valeur de la texture de l'avatar lorsqu'il va dans la direction haut

	if ((Matrice[y - 1][x] != '1')  && (y > 0) && (start == true)) {
		y--;
		if (Matrice[y][x] == '0' ) {
			score++;
			nbBall--;
			Matrice[y][x] = '2';
		}
		if (Matrice[y][x] == '3') {
			takeSuperBall = true;
			Matrice[y][x] = '2';
		}
		//Gestion des coordonn�es de l'avatar pour la t�l�portation
		if (Matrice[y][x] == '4') {
			x = portailAy + 1;
			y = portailAx;
		}
		if (Matrice[y][x] == '5') {
			x = portailBy + 1;  //
			y = portailBx;
		}
		// Incr�mentation du score si le joueur mange un ennemi 
		if (mange == true) {
			score += 100;
			mange = false;
		}
	}
	
	
	
}

void avatar::DepBottom()
{
	img = 18; //On r�cup�re la valeur de la texture de l'avatar lorsqu'il va dans la direction bas

	if ((Matrice[y + 1][x] != '1')  && (y < 21) && (start == true)) {
		y++;
		if (Matrice[y][x] == '0' ) {
			score++;
			nbBall--;
			Matrice[y][x] = '2';
		}
		if (Matrice[y][x] == '3') {
			takeSuperBall = true;
			Matrice[y][x] = '2';
			cout << "MATRICE 3 ";
		}
		//Gestion des coordonn�es de l'avatar pour la t�l�portation
		if (Matrice[y][x] == '4') {
			x = portailAy + 1;
			y = portailAx;
		}
		if (Matrice[y][x] == '5') {
			x = portailBy + 1;  //
			y = portailBx;
		}
		// Incr�mentation du score si le joueur mange un ennemi 
		if (mange == true) {
			score += 100;
			mange = false;
		}
	}
	
	

	
}


//Fonction pour afficher notre avatar. Nous utilisons une texture pour le repr�senter
void avatar::Dessiner()
{
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, texture[img]);
	glBegin(GL_QUADS);
	glColor3d(1.0, 1.0, 1.0);
	glTexCoord2f(1.0f, 1.0f); glVertex2d(x, y);
	glTexCoord2f(0.0f, 1.0f);glVertex2d(x + 1, y);
	glTexCoord2f(0.0f, 0.0f);glVertex2d(x + 1, y + 1);
	glTexCoord2f(1.0f, 0.0f);glVertex2d(x, y + 1);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	glutPostRedisplay();
}



