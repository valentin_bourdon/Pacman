/////////////////////////////////////////////////////////////
//														   //
//		/*PROJET PACMAN - MTI 3D - BOURDON Valentin*/      //
//														   //
//				----- Class Ennemi -----				   //
//														   //
/////////////////////////////////////////////////////////////


#include <sstream>
#include <iostream>
#include <vector>
#include <windows.h> 
#include <time.h>
#include "SOIL/SOIL.h"
#include "GL\glut.h"

class ennemi
{

private :
	int x; //Position x de l'ennemi
	int y; //Position y de l'ennemi
	bool dead; //Booleen pour savoir si l'ennemi est mort ou pas

public:

	ennemi(int,int);
	
	//Fonction qui g�re la mort de l'ennemi et qui renvoie cette valeur//
	void UpdateVivant();
	void UpdateMort();
	bool Mort();
	
	//Fonctions qui renvoies les positions x et y de l'ennemi//
	int PositionX();
	int PositionY();

	//Fonctions pour le d�placement de l'ennemi
	void DepGauche();
	void DepDroite();
	void DepTop();
	void DepBottom();


	//Fonction pour g�rer al�atoirement le d�plcament de l'ennemi//
	void DeplacementEnnemi(int, int, int, int);

	//Fonction qui reset la position de l'ennemi//
	void ResetPos(int, int);

	//Fonction pour afficher l'ennemi//
	void Dessiner(int);
};



