
# Pacman

Pacman developed in C++ and OpenGL.

# Requirements

- Visual Studio

# Installation

:one: Clone this repository

:two: Open the visual studio project `Pacman.sln`

:three: Add dependencies:
- `Project > Properties > C/C++ > General > Additional Include Directories`
	- Add **Pacman/GL** folder path
- `Project > Properties > Linkers > Input`
	- Add in `Additional dependencies` : **glut32.lib** and **SOIL.lib**
- `Project > Properties > Linkers > General`
	- Add in `Additional Library Directories` : **Pacman/LIB** folder path
- `C:\Windows\System32`
	- Copy/Paste **glut32.dll**
	
:four: Run
- Release - x86

# License

All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
